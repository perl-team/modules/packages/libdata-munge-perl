libdata-munge-perl (0.111-1) unstable; urgency=medium

  * Import upstream version 0.111.
  * Update years of upstream copyright.

 -- gregor herrmann <gregoa@debian.org>  Mon, 29 Jul 2024 02:09:06 +0200

libdata-munge-perl (0.101-1) unstable; urgency=medium

  * Import upstream version 0.101.
  * Add test dependency on libtest2-suite-perl.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.7.0.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Sun, 14 Apr 2024 00:30:57 +0200

libdata-munge-perl (0.097-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libdata-munge-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 19 Nov 2022 21:23:48 +0000

libdata-munge-perl (0.097-2) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Xavier Guimard ]
  * Email change: Xavier Guimard -> yadd@debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 12 Jun 2022 23:06:28 +0100

libdata-munge-perl (0.097-1) unstable; urgency=medium

  * Team upload

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: use HTTPS for GitHub URLs.

  [ Damyan Ivanov ]
  * New upstream version 0.097
  * declare conformance with Policy 4.1.1 (no changes needed)

 -- Damyan Ivanov <dmn@debian.org>  Mon, 20 Nov 2017 17:27:18 +0000

libdata-munge-perl (0.096-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ Lucas Kanashiro ]
  * Import upstream version 0.096
  * d/u/metadata: update Repository
  * Bump debhelper compatibility level to 9
  * Declare compliance with Debia policy 3.9.7
  * debian/control: wrap and sort

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Sun, 13 Mar 2016 23:26:26 -0300

libdata-munge-perl (0.095-1) unstable; urgency=medium

  * Team upload

  [ Nuno Carvalho ]
  * New upstream release
  * debian/copyright: update copyright years
  * debian/control: add "Testsuite: autopkgtest-pkg-perl"

  [ gregor herrmann ]
  * Drop build dependencies on libtest-pod*.
    The respective tests are gone.

 -- Nuno Carvalho <smash@cpan.org>  Thu, 21 May 2015 22:08:48 +0100

libdata-munge-perl (0.08-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata
  * Imported upstream version 0.08
  * Update years of upstream and packaging copyright.
  * Add build-dependency on libtest-warnings-perl.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Sat, 11 Oct 2014 16:33:47 +0200

libdata-munge-perl (0.07-1) unstable; urgency=low

  * New upstream release.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Mon, 28 Oct 2013 23:18:43 +0100

libdata-munge-perl (0.06-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Xavier Guimard ]
  * Imported Upstream version 0.06
  * Update debian/copyright (years and format)
  * Bump Standards-Version to 3.9.4

 -- Xavier Guimard <x.guimard@free.fr>  Thu, 07 Mar 2013 17:11:08 +0100

libdata-munge-perl (0.04-1) unstable; urgency=low

  * Initial release (closes: #648365).

 -- gregor herrmann <gregoa@debian.org>  Thu, 10 Nov 2011 22:55:45 +0100
